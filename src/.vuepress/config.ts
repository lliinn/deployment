import { defineUserConfig } from "vuepress";
import { hopeTheme } from "vuepress-theme-hope";
// import { getDirname, path} from "@vuepress/utils";
// import { config } from "docs-shared";
import theme from "./theme.js";

export default defineUserConfig({
  lang: "zh-CN",
  title: "Lysz Oiers' WIKI!!!",
  description: "Lysz Oiers' WIKI!!!",
  pagePatterns: [
    "**/*.md",
    "!**/*.snippet.md",
    "!.vuepress",
    "!node_modules",
  ],

  theme: hopeTheme({
    logo: "https://vuejs.org/images/logo.png",
    
  }),
});
